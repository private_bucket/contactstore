/**
 * 
 */
package com.assignments.gingerpayments.test;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;

import com.assignments.gingerpayments.entities.Contact;
import com.assignments.gingerpayments.entities.Group;
import com.assignments.gingerpayments.exceptions.ContactServiceAPIException;
import com.assignments.gingerpayments.service.ContactService;
import com.assignments.gingerpayments.service.impl.ContactServiceImpl;

/**
 * Contact management application which should perform required basic 
 * functionality of managing persons contacts.
 */
public class ContactStore {

	static Scanner scanner;
	private static final String EMAIL = "email";
	private static final String PHONE = "phone";
	private static final String STREET_ADDRESS = "street address";
	
	private static ContactService contactService;
	
	{
		contactService = new ContactServiceImpl();
		scanner = new Scanner(System.in);
	}
	
	public static void main(String[] args) 
	{
		ContactStore contactStore = new ContactStore();
		contactStore.recurssify();
	}
	
	private void recurssify()
	{
		System.out.println("################################### Contact Store ##############################");
		System.out.println("	(1) Add a contact");
		System.out.println("	(2) Add a group");
		System.out.println("	(3) Get contacts by First Name and/or Last Name ");
		System.out.println("	(4) Get contacts by email");
		System.out.println("	(5) Get all contacts of a group");
		System.out.println("	(6) Get all groups of a contact");
		System.out.println("	(7) Add a contact to a group");
		System.out.println("	(8) Exit contact store");
		System.out.println("################################### Contact Store ##############################");
		
		System.out.print("Please select option : ");
		try
		{
			Integer option = scanner.nextInt();
			switch(option)
			{
			case 1 : initiateAddContactProcess();
					 recurssify();
					 break;
			case 2 : initiateAddGroupProcess();
					 recurssify();
			 		 break;
			case 3 : searchContactByFirstNameAndOrLastName();
					 recurssify();
			 		 break;
			case 4 : searchContactsByEmail();
					 recurssify();
			 		 break; 
			case 5 : searchContactsByGroup();
					 recurssify();
			 		 break; 
			case 6 : searchGroupByContact();
					 recurssify();
					 break; 
			case 7 : initiateAddContactToGroupProcess();
					 recurssify();
					 break;	
			case 8 : System.out.println("Thanks for using contact store, exiting....");
					 System.exit(0);
			
			default :
					System.out.println("It seems you have provided an invalid input, Closing!");
			}
		}
		catch(InputMismatchException e)
		{
			System.out.println("It seems you have provided an invalid input, please re-try !");
			recurssify();
		}
		catch(ContactServiceAPIException e)
		{
			System.out.println("Error Code : "+ e.getErrorCode());
			System.out.println("Error Reason : "+ e.getMessage());
			recurssify();
		}
	}
	
	private static void initiateAddContactToGroupProcess() throws ContactServiceAPIException {
		
		System.out.println("---------------------------------- All Groups ---------------------------------");
		for(Group group : contactService.getGroups())
		{
			System.out.println(group.getName());
		}
		System.out.println("");
		System.out.print("Enter which group (group ID) to add contact into : ");
		Integer groupId = scanner.nextInt();
		Set<Contact> contacts = contactService.getContacts();
		System.out.println("---------------------------------- All Contacts -------------------------------");
		for(Contact contact : contacts)
		{
			System.out.println(contact);
		}
		
		System.out.print("Select which contact (Contact ID)  to add to this group : ");
		Integer contactId = scanner.nextInt();

		Boolean isGrouped = contactService.addToGroup(groupId, contactId);
		if(isGrouped) System.out.println("Contact added to group");
		else System.out.println("Failed to add contact to group");
	}

	private static void searchGroupByContact() throws ContactServiceAPIException {
		
		Set<Contact> contacts = contactService.getContacts();
		System.out.println("---------------------------------- All Contacts ------------------------------");
		for(Contact contact : contacts)
		{
			System.out.println(contact);
		}
		
		System.out.print("Select contact (contact ID) to find all it's group : ");
		Integer contactId = scanner.nextInt();

		Set<Group> groups = contactService.getGroupsByContact(contactId);
		if(groups.size() > 0)
		{
			for(Group group : groups)
			{
				System.out.println(group.getName());
			}
		}
		else
		{
			System.out.println("No group found");
		}
	}

	private static void searchContactsByEmail() throws ContactServiceAPIException {
		
		System.out.print("Enter email : ");
		String email = scanner.next();
		
		Set<Contact> matchedElements = contactService.getContacts(email);
		if(matchedElements.size() > 0)
		{
			for(Contact contact : matchedElements)
			{
				System.out.println(contact);
			}
		}
		else
		{
			System.out.println("No contact found");
		}
	}

	private static void searchContactByFirstNameAndOrLastName() throws ContactServiceAPIException {
		
		System.out.print("Enter First Name : ");
		String firstName = scanner.next();
		System.out.print("Enter Last Name : ");
		String lastName = scanner.next();
		
		Set<Contact> matchedElements = contactService.getContacts(firstName, lastName);
		if(matchedElements.size() > 0)
		{
			for(Contact contact : matchedElements)
			{
				System.out.println(contact);
			}
		}
		else
		{
			System.out.println("No contact found");
		}
		
	}
	
	private void searchContactsByGroup() throws ContactServiceAPIException {
		
		Set<Group> groups = contactService.getGroups();
		System.out.println("---------------------------------- All Groups --------------------------------");
		for(Group group : groups)
		{
			System.out.println(group);
		}
		
		System.out.print("Select group (group ID) to find all it's contacts : ");
		Integer groupId = scanner.nextInt();

		Set<Contact> contacts = contactService.getContactsByGroup(groupId);
		
		if(contacts.size() > 0)
		{
			for(Contact contact : contacts)
			{
				System.out.println(contact);
			}
		}
		else
		{
			System.out.println("No contacts found in this group");
		}
	}

	private static void initiateAddGroupProcess() throws ContactServiceAPIException {
		System.out.print("Enter group name : ");
		Group group = contactService.addGroup(scanner.next());
		System.out.println("Following group added successfully");
		System.out.println(group);
	}

	private static void initiateAddContactProcess() throws ContactServiceAPIException {
		
		System.out.print("Enter First Name : ");
		String firstName = scanner.next();
		System.out.print("Enter Last Name : ");
		String lastName = scanner.next();
		Set<String> emails = new HashSet<>();
		addMoreEntity("Enter Email : ", emails, EMAIL);
		Set<String> phones = new HashSet<>();
		addMoreEntity("Enter phone : ", phones, PHONE);
		Set<String> streetAdress = new HashSet<>();
		addMoreEntity("Enter street address : ", streetAdress, STREET_ADDRESS);
		
		Contact contact = contactService.addContact(firstName, lastName, phones, emails, streetAdress);
		System.out.println("Following contact added successfully");
		System.out.println(contact);
	}
	
	private static void addMoreEntity(String inputDescription, Set<String> dataStore, String entity)
	{
		System.out.print(inputDescription);
		dataStore.add( scanner.next() );
		
		System.out.print("Add another "+entity+"? (yes/no) : ");
		String option = scanner.next();
		if("yes".equalsIgnoreCase(option))
		{
			addMoreEntity(inputDescription, dataStore, entity);
		}
	}
}
