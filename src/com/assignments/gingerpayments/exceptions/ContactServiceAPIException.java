/**
 * 
 */
package com.assignments.gingerpayments.exceptions;

/**
 * Generic exception to be thrown to process exception scenarios.
 * Exception specific to use-case or entity can be added and must be extended by
 * this exception
 */
public class ContactServiceAPIException extends Exception {

	private static final long serialVersionUID = 43877608612802328L;
	
	private String message;
	private ErrorCode errorCode;
	
	public ContactServiceAPIException(String message, ErrorCode errorCode)
	{
		this.message = message;
		this.errorCode = errorCode;
	}
	
	public String getMessage()
	{
		return this.message;
	}

	public ErrorCode getErrorCode()
	{
		return this.errorCode;
	}
	
	
	public enum ErrorCode {
		
		DUPLICATE_CONTACT(1001),
		DUPLICATE_GROUP(1002),
		DUPLICATE_ENTITY(1003),
		MANDATORY(1004);
		
		Integer errorCode;
		
		private ErrorCode(Integer errorCode){
			this.errorCode = errorCode;
		}
	}

}
