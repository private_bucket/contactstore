/**
 * 
 */
package com.assignments.gingerpayments.service.impl;

import java.util.HashSet;
import java.util.Set;

import com.assignments.gingerpayments.entities.Contact;
import com.assignments.gingerpayments.entities.Group;
import com.assignments.gingerpayments.exceptions.ContactServiceAPIException;
import com.assignments.gingerpayments.exceptions.ContactServiceAPIException.ErrorCode;
import com.assignments.gingerpayments.service.ContactService;

/**
 * {@inheritDoc}
 */
public class ContactServiceImpl implements ContactService {

	private static Integer contactId = 1;
	private static Integer groupId = 1;

	private Set<Contact> contacts;
	private Set<Group> groups;

	{
		contacts = new HashSet<>();
		groups = new HashSet<>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Contact addContact(String firstName, String lastName, Set<String> phone, Set<String> email,
			Set<String> streetAddress) throws ContactServiceAPIException {

		if(firstName == null || lastName == null) throw new ContactServiceAPIException("Contact first and last name is mandatory", ErrorCode.MANDATORY);
		if(phone == null || phone.size() == 0)  throw new ContactServiceAPIException("Atleast one phone number is required", ErrorCode.MANDATORY);
		if(email == null || email.size() == 0)  throw new ContactServiceAPIException("Atleast one email is required", ErrorCode.MANDATORY);
		if(streetAddress == null || streetAddress.size() == 0)  throw new ContactServiceAPIException("Atleast one streetAddress is required", ErrorCode.MANDATORY);

		Contact contact = new Contact(firstName, lastName, streetAddress, phone, email);
		contact.setId(contactSequenceGenerator());
		this.contacts.add(contact);

		return contact;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Contact getContact(Integer id) throws ContactServiceAPIException {

		if(id == null) throw new ContactServiceAPIException("Contact to be searched can not be null", ErrorCode.MANDATORY);

		for(Contact contact : this.contacts)
		{
			if(id == contact.getId() )
			{
				return contact;
			}
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Contact> getContacts(String firstName, String lastName) throws ContactServiceAPIException {

		Set<Contact> matchedElements = new HashSet<>();

		for(Contact contact : this.contacts)
		{
			//either find based on both
			if(firstName != null && lastName != null)
			{
				if(firstName.equalsIgnoreCase( contact.getFirstName() ) && lastName.equalsIgnoreCase( contact.getLastName() ))
				{
					matchedElements.add(contact);
				}
			}
			// only first name 
			else if(firstName != null && firstName.equalsIgnoreCase( contact.getFirstName() ))
			{
				matchedElements.add(contact);
			}
			// only last name
			else if(lastName != null && lastName.equalsIgnoreCase( contact.getLastName() ))
			{
				matchedElements.add(contact);
			}
		}

		return matchedElements;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Contact> getContacts(String email) throws ContactServiceAPIException {

		if(email == null || "".equals( email )) throw new ContactServiceAPIException("Email can not be null", ErrorCode.MANDATORY);

		Set<Contact> matchedElements = new HashSet<>();
		for(Contact contact : this.contacts)
		{
			for(String contactEmail : contact.getEmails())
			{
				if(contactEmail.startsWith(email))
				{
					matchedElements.add(contact);
				}
			}
		}

		return matchedElements;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Contact> getContacts() throws ContactServiceAPIException {
		return this.contacts;
	}
	
	@Override
	public Set<Contact> getContactsByGroup(Integer groupId) throws ContactServiceAPIException {
		
		Group group = getGroup(groupId);

		if(group == null) throw new ContactServiceAPIException("No such group exists", ErrorCode.MANDATORY);
		
		return group.getContacts();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Group addGroup(String name) throws ContactServiceAPIException {

		if(name == null || "".equals( name )) throw new ContactServiceAPIException("Group name can not be null", ErrorCode.MANDATORY);

		//check if a group with given name already exists
		Group group = getGroup(name);

		if(group != null)
		{
			throw new ContactServiceAPIException("Duplicate group. A group with given name already exists", ErrorCode.DUPLICATE_ENTITY);
		}

		group = new Group(name);
		group.setId(groupSequenceGenerator());
		this.groups.add(group);

		return group;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Group getGroup(Integer id) throws ContactServiceAPIException {

		if(id == null) throw new ContactServiceAPIException("Group to be searched can not be null", ErrorCode.MANDATORY);

		for(Group group : this.groups)
		{
			if(id.equals(group.getId()))
			{
				return group;
			}
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Group getGroup(String name) throws ContactServiceAPIException {

		if(name == null || "".equals( name )) throw new ContactServiceAPIException("Group name can not be null", ErrorCode.MANDATORY);

		for(Group group : this.groups)
		{
			if(name.equalsIgnoreCase(group.getName()))
			{
				return group;
			}
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Group> getGroups() throws ContactServiceAPIException {
		return this.groups;
	}
	
	@Override
	public Set<Group> getGroupsByContact(Integer contactId) throws ContactServiceAPIException {

		Contact contact = getContact(contactId);

		if(contact == null) throw new ContactServiceAPIException("No such contact exists in the contact store", ErrorCode.MANDATORY);
		
		return contact.getGroups();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean addToGroup(Integer groupId, Integer contactId) throws ContactServiceAPIException {

		if(groupId == null) throw new ContactServiceAPIException("Group can not be null", ErrorCode.MANDATORY);

		if(contactId == null) throw new ContactServiceAPIException("Contact to be added can not be null", ErrorCode.MANDATORY);

		//check if the given contact already exists in the selected group
		Contact contact = getContact(contactId);

		Boolean isGrouped = false;

		Group group = getGroup(groupId);
		if(group != null)
		{
			if(contact != null)
			{
				for(Group contactsGroup : contact.getGroups())
				{
					if(contactsGroup.getId() == groupId)
						throw new ContactServiceAPIException("Duplicate contact. This contact already exists in this group", ErrorCode.DUPLICATE_CONTACT);
				}

				group.getContacts().add(contact);
				isGrouped = true;
				contact.getGroups().add(group);
			}
			else
			{
				throw new ContactServiceAPIException("No such contact exists in the contact store", ErrorCode.MANDATORY);
			}

		}
		else
		{
			throw new ContactServiceAPIException("No such group exists in the contact store", ErrorCode.MANDATORY);
		}

		return isGrouped;
	}

	private static synchronized Integer contactSequenceGenerator()
	{
		return contactId ++ ;
	}

	private static synchronized Integer groupSequenceGenerator()
	{
		return groupId ++ ;
	}
}
