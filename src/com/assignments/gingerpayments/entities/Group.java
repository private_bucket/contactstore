/**
 * 
 */
package com.assignments.gingerpayments.entities;

import java.util.HashSet;
import java.util.Set;

/**
 * Group entity which contains basic details of a persons contact.
 * <br/>
 * <ol>
 * 	    <li>Group Name</li>
 * 		<li>List of {@link contacts} </li>
 * </ol>
 */
public class Group {

	private Integer id;
	private String name;
	private Set<Contact> contacts;

	{
		contacts = new HashSet<>();
	}
	
	public Group(String name)
	{
		this.name = name;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<Contact> getContacts() {
		return contacts;
	}
	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Group [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", contacts=[");
		for(Contact contact : contacts)
		{
			builder.append("[");
			builder.append(contact.getFirstName()).append(" ").append(contact.getLastName()).append("( ID : ").append(contact.getId()).append(")");
			builder.append("]");
		}
		builder.append("]");
		builder.append("]");
		return builder.toString();
	}
}
