/**
 * 
 */
package com.assignments.gingerpayments.entities;

import java.util.HashSet;
import java.util.Set;

/**
 * Contact entity which contains basic details of a persons contact.
 * <br/>
 * <ol>
 * 	    <li>First Name</li>
 * 		<li>Last Name</li>
 * 		<li>List of Street Address</li>
 * 		<li>List of phone numbers</li>
 * 		<li>List of email</li>
 * </ol>
 */
public class Contact 
{
	private Integer id;
	private String firstName;
	private String lastName;
	private Set<String> streetAddresses;
	private Set<String> phones;
	private Set<String> emails;
	private Set<Group> groups;
	
	{
		streetAddresses = new HashSet<>();
		phones =  new HashSet<>();
		emails =  new HashSet<>();
		groups =  new HashSet<>();
	}
	
	public Contact(String firstName, String lastName, Set<String> streetAddresses, Set<String> phones, Set<String> emails)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.streetAddresses = streetAddresses;
		this.phones = phones;
		this.emails = emails;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Set<String> getStreetAddresses() {
		return streetAddresses;
	}
	public void setStreetAddresses(Set<String> streetAddresses) {
		this.streetAddresses = streetAddresses;
	}
	public Set<String> getPhones() {
		return phones;
	}
	public void setPhones(Set<String> phones) {
		this.phones = phones;
	}
	public Set<String> getEmails() {
		return emails;
	}
	public void setEmails(Set<String> emails) {
		this.emails = emails;
	}
	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Contact [id=");
		builder.append(id);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", streetAddresses=");
		builder.append(streetAddresses);
		builder.append(", phones=");
		builder.append(phones);
		builder.append(", emails=");
		builder.append(emails);
		builder.append(", groups= [");
		
		for(Group group : groups)
		{
			builder.append("[");
			builder.append(group.getName()).append("( ID : ").append(group.getId()).append(")");
			builder.append("]");
		}
		builder.append("]");
		builder.append("]");
		return builder.toString();
	}
	
	
}
